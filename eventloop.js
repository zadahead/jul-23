
const Comp = () => {
    let state = 0;

    const setState = (newState) => {
        state = newState;
    } 
    const useState = () => {
        return state;
    }
    const render = (flag) => {
        const val = useState();

        if(flag) {
            setTimeout(() => {
                console.log(val)
            }, 2000);
        } else {
            console.log(val)
        }
    }

    return {
        setState,
        useState,
        render
    }
}

const comp = Comp();

comp.render(true);

comp.setState(1);

comp.setState(2);

comp.setState(3);

comp.setState(4);


comp.setState(5);

comp.render();



// comp.setState(2);
// comp.render();

// comp.setState(3);
// comp.render();

// comp.setState(4);
// comp.render(true);

// comp.setState(5);
// comp.render();