import { forwardRef } from "react";
import "./Input.css";

export const Input = forwardRef((props, ref) => {
  const handleChange = (e) => {
    if (props.onChange) {
      props.onChange(e.target.value);
    }
  };

  return (
    <div className="Input">
      <input ref={ref} value={props.value} onChange={handleChange} />
    </div>
  );
});
