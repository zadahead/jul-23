import { Line } from "../../Layouts/Line/Line";
import { Icon } from "../Icon/Icon";
import "./Btn.css";
import { innerFunc } from "./logic";



export const Btn = (props) => {
  const handleClick = (e) => {
    //console.log("parent clicked");
    innerFunc();

    if(props.onClick) {
      props.onClick(e);
    }
  };

  return (
    <button className="Btn" data-testid="btn" onClick={handleClick}>
      <Line>
        {props.i && <Icon i={props.i} />}
        {props.children}
      </Line>
    </button>
  );
};
