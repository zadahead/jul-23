import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import * as LogicModule from './logic';


import { Btn } from "./Btn";

const innerFuncSpy = jest.spyOn(LogicModule, 'innerFunc');

describe("<Btn />", () => {
    it('will test btn is rendered', () => {
        render(<Btn />);

        const btn = screen.getByRole('button');

        expect(btn).toBeInTheDocument();

        const btnTestId = screen.getByTestId('btn');

        expect(btnTestId).toBeInTheDocument();
    })

    it('will test btn is rendered', () => {
        const func = jest.fn();
        render(<Btn onClick={func}>Click me</Btn>);

        const btn = screen.getByRole('button');

        fireEvent.click(btn);

        expect(func).toBeCalledTimes(1);
        expect(innerFuncSpy).toBeCalledTimes(1);
    })
})