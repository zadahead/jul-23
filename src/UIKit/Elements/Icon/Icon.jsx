

export const Icon = (props) => {
    const allowed = ['home', 'favorite'];
    if(!props.i || !allowed.includes(props.i)) {
        return <span>not allowed</span>
    }
  return <span className="material-symbols-rounded">{props.i}</span>;
};
