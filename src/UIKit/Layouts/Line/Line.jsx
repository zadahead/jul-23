import './Line.css';

export const Line = (props) => {
    return (
        <div className={`Line ${props.className || ''}`}>
            {props.children}
        </div>
    )
}

export const Between = (props) => {
    return (
        <Line className="Between">
            {props.children}
        </Line>
    )
}


export const Rows = (props) => {
    return (
        <Line className="Rows">
            {props.children}
        </Line>
    )
}