import { configureStore } from "@reduxjs/toolkit";
import { Provider } from "react-redux";

import counterReducer from './countSlice';

const store = configureStore({
    reducer: {
        counter: counterReducer
    }
})

export const Redux = ({ children }) => {
    return (
        <Provider store={store}>
            { children }
        </Provider>
    )
}