import { useEffect } from "react";
import { Line, Rows } from "../UIKit/Layouts/Line/Line";
import { Btn } from "../UIKit/Elements/Btn/Btn";

import { useState } from "react";
import axios from "axios";

const useFetch = (url) => {
  const [data, setData] = useState(null); //
  const [isLoading, setIsLoading] = useState(true); //
  const [error, setError] = useState('');

  useEffect(() => {
    axios.get(url)
      .then((resp) => {
        setTimeout(() => {
          setIsLoading(false);
          setData(resp.data);
        }, 1500);
      })
      .catch(err => {
        setIsLoading(false);
        setError(err.message);
      })
  }, []);

  return {
    data,
    isLoading,
    error
  }
}

const Fetch = (props) => {
  const { data, isLoading, error } = useFetch(props.url);

  if(isLoading) {
    return <Line>{isLoading && <h3>loading..</h3>}</Line>;
  }

  if(error) {
    return <h3 style={{ color: 'red'}}>{error}</h3>
  }


  return props.onRender(data);
}

export const LifeCycle = () => {
  const [list, setList] = useState([]);

  const [count, setCount] = useState(0);

  const handleAdd = () => {
    setCount(count + 1);
  };

  // useEffect(() => {
  //   if (data) {
  //     setList(data);
  //   }
  // }, [data])

  useEffect(() => {
    //console.log('mounted')


    document.body.addEventListener("click", handleBodyClick);

    return () => {
      console.log("Unmounted");
      document.body.removeEventListener("click", handleBodyClick);
    };
  }, []);


  const handleBodyClick = () => {
    //console.log("Body click")
  };

 

  const handleToggle = (item) => {
    // item.completed = !item.completed;
    // setList([...list]);
  };

  const getStyle = (item) => {
    if (item.completed) {
      return {
        color: "#e1e1e1",
        textDecoration: "line-through",
      };
    }
    return {};
  };
  

  return (
    <Rows>
      <h1>Todos list</h1>
      <Fetch 
        url="https://jsonplaceholder.typicode.com/todos" 
        onRender={(data) => {
          return data.map((item) => {
            return (
              <h2 key={item.id} style={getStyle(item)}>
                <span>{item.title}</span>
              </h2>
            );
          })
        }}
      />
    </Rows>
  );
};


export const UsersLifeCycle = () => {
  return (
    <Rows>
      <h1>Users list</h1>
      <Fetch 
        url="https://jsonplaceholder.typicode.com/users"
        onRender={(data) => {
          return data.map((user) => {
            return (
              <h2 key={user.id}>
                <span>{user.name} {user.email}</span>
              </h2>
            );
          })
        }}
      />
    </Rows>
  )
}

