import { ColorSwitchH } from "Components/ColorSwitchH"
import { CounterH } from "Components/CounterH"
import { Rows } from "UIKit"

export const CHooks = () => {
    return (
        <Rows>
            <h1>Custom Hooks</h1>
            <ColorSwitchH />
        </Rows>
    )
}