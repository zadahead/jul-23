import { useState } from "react";
import { Counter } from "../Components/Counter";
import { Line, Rows } from "../UIKit/Layouts/Line/Line";
import { Btn } from "../UIKit/Elements/Btn/Btn";
import { LifeCycle } from "./LifeCycle";

export const Home = () => {
  const [isDisplay, setIsDisplay] = useState(true);

  const handleToggle = () => {
    setIsDisplay(!isDisplay);
  }
  return (
    <Rows>
      <Line>
        <h1>Welcome Home</h1>
        <Btn onClick={handleToggle}>Toggle</Btn>
      </Line>
      {isDisplay && <LifeCycle />}
    </Rows>
  );
};
