import { Link } from "react-router-dom";
import { Btn } from "../UIKit/Elements/Btn/Btn";
import { Icon } from "../UIKit/Elements/Icon/Icon";
import { Line, Rows } from "../UIKit/Layouts/Line/Line";
import { useContext } from "react";
import { countContext } from "Context/countContext";
import { colorSwitchContext } from "Context/colorSwitchContext";
import { useDispatch } from "react-redux";
import { counterActions } from "Store/countSlice";

export const About = () => {
  console.log("About")
  const { count, setCount } = useContext(countContext);
  const { handleSwitch, setColorA } = useContext(colorSwitchContext);

  const dispatch = useDispatch();


  const handleClick = () => {
    console.log("CLICKED!");
    setCount(count + 1);
    handleSwitch();
  };

  const handleChangeColorA = () => {
    setColorA("Yellow");
  }

  const handleReduxAdd = () => {
    dispatch(counterActions.increment())
  }

  return (
    <Rows>
      <h1>About Page</h1>
      <Line>
        <Btn onClick={handleClick}>Click me!!!</Btn>
        <Btn onClick={handleChangeColorA}>Change Color</Btn>
        <Btn onClick={handleReduxAdd}>Redux Add</Btn>

        <Icon i="favorite" />
        <Link to='/home'>
          <Btn i="home">Go home</Btn>
        </Link>
      </Line>
    </Rows>
  );
};
