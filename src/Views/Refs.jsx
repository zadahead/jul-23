import { useEffect, useRef, useState } from "react";

import { Line, Rows, Btn, Input } from "UIKit";

export const Refs = () => {
  const [count, setCount] = useState({ value: 0});
  const countRef = useRef();
  countRef.current = count.value;
  console.log('render')

  const inputRef = useRef();

  const h2Ref = useRef();


  useEffect(() => {

    setTimeout(() => {
      setCount({ value: countRef.current + 1});
    }, 3000);
  }, []);

  useEffect(() => {
    if(count.value > 3) {
        h2Ref.current.style.color = 'blue';

    }
  }, [count])

  const handleAdd = () => {
    setCount({ value: count.value + 1});
  };

  const handleShow = () => {
    console.log(inputRef.current.value);
  }
  return (
    <Rows>
      <Line>
        <h1 ref={h2Ref} className="h1">UseRef, {count.value}</h1>
        <Btn onClick={handleAdd}>Add</Btn>
      </Line>
      <Line>
        <Input ref={inputRef} />
        <Btn onClick={handleShow}>Show</Btn>
      </Line>
    </Rows>
  );
};
