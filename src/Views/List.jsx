import { useState } from "react";
import { Line, Rows } from "../UIKit/Layouts/Line/Line";
import { Btn } from "../UIKit/Elements/Btn/Btn";

const usersList = [
  { id: 1, name: "mosh" },
  { id: 2, name: "david" },
  { id: 3, name: "ruth" },
];

export const List = () => {
  const [users, setUsers] = useState(usersList);
  const [count, setCount] = useState(0);
  
  const handleSet = () => {
    setCount(20);
  }

  const handleAdd = () => {
    users.push({
      id: crypto.randomUUID(),
      name: 'baruch'
    })
    setUsers([...users]);
  }
  return (
    <Rows>
      <Line>
        <h1>Users List</h1>
        <Btn onClick={handleAdd}>Add</Btn>
        <Btn onClick={handleSet}>Set</Btn>
      </Line>
      <ul>
        {users.map((user, i) => {
          return <h2 key={user.id}>{user.name}</h2>;
        })}
      </ul>
    </Rows>
  );
};
