import { useState } from "react";

export const useColorSwitch = () => {
    const [isColorA, setIsColorA] = useState(true);

    const handleSwitch = () => {
        setIsColorA(!isColorA);
    }

    return {
        isColorA, 
        handleSwitch
    }
}