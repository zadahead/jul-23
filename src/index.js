import React from "react";
import ReactDOM from "react-dom/client";

import { BrowserRouter } from "react-router-dom";
import { App } from "./App";
import { CountProvider } from "Context/countContext";
import { ColorSwitchProvider } from "Context/colorSwitchContext";
import { Redux } from "Store/store";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Redux>
    <ColorSwitchProvider>
      <CountProvider>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </CountProvider>
    </ColorSwitchProvider>
  </Redux>
);
