import { useState } from "react";
import { Btn } from "../UIKit/Elements/Btn/Btn";
import { Line, Rows } from "../UIKit/Layouts/Line/Line";
import { Input } from "../UIKit/Elements/Input/Input";

export const Counter = () => {
  const COLORA = "red";
  const COLORB = "blue";

  const [count, setCount] = useState(10);
  const [color, setColor] = useState(COLORA);



  const handleAdd = () => {
    setCount(count + 1);
  };

  const handleColorSwitch = () => {
    setColor(color === COLORB ? COLORA : COLORB);
  };

  const handleInputChange = (value) => {
    setCount(+value);
  }


  const styleCss = {
    color: color,
    backgroundColor: "yellow",
    padding: "10px",
  };

  return (
    <Rows>
      <h2 style={styleCss}>Count, {count}</h2>
      <Input value={count} onChange={handleInputChange}  />
      <div>
        <Line>
          <Btn onClick={handleAdd}>Add</Btn>
          <Btn onClick={handleColorSwitch}>Switch Color</Btn>
        </Line>
      </div>
    </Rows>
  );
};
