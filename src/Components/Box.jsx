export const Box = (props) => {

    const containerCss = {
        border: '1px solid #e1e1e1',
        padding: '20px',
        margin: '10px',
        backgroundColor: '#f1f1f1'
    }

    const contentCss = {
        padding: '20px',
        backgroundColor: '#ddd'
    }

    return (
        <div style={containerCss}>
            <h1>{props.title}</h1>
            <h2>{props.info}</h2>
            <div style={contentCss}>
               {props.children}
            </div>
        </div>
    )
}