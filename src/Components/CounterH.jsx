import { useCounter } from "Hooks/useCounter";
import { Btn, Rows } from "UIKit";

/*
    Create Custom Hook

    1) create a fully working component
    2) separate logic and render
    3) create custom hook function "useXxx"
    4) move all logic, to the new "use" hook
    5) find what the render function needs and return that from hook
    6) get these values from "use" function 
    7) separate to other file and export it
*/


export const CounterH = () => {
    //logic
   const { count, handleAdd } = useCounter(10);

    //render
    return (
        <Rows>
            <h2>Count, {count}</h2>
            <Btn onClick={handleAdd}>Add</Btn>
        </Rows>
    )
}