import { Btn, Rows } from "UIKit";
import { memo, useCallback, useMemo, useState } from "react"

export const Callback = () => {
    console.log("Callback")
    const [count, setCount] = useState(0);
    const handleAdd = () => {
        setCount(count + 1);
    }

    const value = count > 5 ? 'hello' : 'daviddd';

    const logThis = useCallback(() => {
        console.log('logThis', count)
    }, [value])

    const calcNum = () => {
        let result = value.length + 1;
        for (let i = 1; i < 1000000000; i++) {
            result++;
        }
        return result;
    }

    const result = useMemo(calcNum, [value]);

    return (
        <Rows>
            <h1>Callback, {count} - {result}</h1>
            <Btn onClick={handleAdd}>Add</Btn>
            <CallbackInner value={value} logThis={logThis} />
        </Rows>
    )
}

const CallbackInner = memo(({value, logThis}) => {
    console.log("CallbackInner")

    logThis();

    return (
        <h3>hello - {value}</h3>
    )
})