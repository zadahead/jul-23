import { useColorSwitch } from "Hooks/useColorSwitch";
import { useCounter } from "Hooks/useCounter";
import { Btn, Rows } from "UIKit";


export const ColorSwitchH = () => {
    //logic
   const { isColorA, handleSwitch } = useColorSwitch();
   const { count, handleAdd} = useCounter();

     //render
    const styleCss = {
        backgroundColor: isColorA ? 'red' : 'blue',
        color: "#fff"
    }

   
    return (
        <Rows>
            <h3 style={styleCss}>Color Switch, {count}</h3>
            <Btn onClick={handleSwitch}>Switch</Btn>
            <Btn onClick={handleAdd}>Add</Btn>
        </Rows>
    )
}