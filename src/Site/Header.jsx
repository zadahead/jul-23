import { NavLink } from "react-router-dom";
import { Between, Line } from "../UIKit/Layouts/Line/Line";
import "./site.css";
import { Icon } from "../UIKit/Elements/Icon/Icon";
import { useContext } from "react";
import { countContext } from "Context/countContext";
import { colorSwitchContext } from "Context/colorSwitchContext";
import { useSelector } from "react-redux";

export const Header = () => {
  const { count } = useContext(countContext);
  const { color } = useContext(colorSwitchContext);

  const store = useSelector((store) => store.counter);
  console.log("store", store);

  return (
    <div className="Header">
      <Between>
        <Line>
          <h2>Jul23</h2>
          <Icon i="favorite" />
          <h3 style={{ color: "#fff", backgroundColor: color }}>{count}</h3>
          <h3>{store.count}</h3>

        </Line>
        <Line>
          <NavLink to="/home">home</NavLink>
          <NavLink to="/about">about</NavLink>
          <NavLink to="/list">list</NavLink>
          <NavLink to="/lifecycle">cycle</NavLink>
          <NavLink to="/refs">refs</NavLink>
          <NavLink to="/hooks">hooks</NavLink>
          <NavLink to="/callback">callback</NavLink>
        </Line>
      </Between>
    </div>
  );
};
