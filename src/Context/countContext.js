import { createContext, useState } from "react";


export const countContext = createContext({});

const Provider = countContext.Provider;

export const CountProvider = ({ children }) => {
    const [count, setCount] = useState(10);

    const value = {
        count,
        setCount,
        mosh: "Hello"
    }
    
    return (
        <Provider value={value}>
            { children }
        </Provider>
    )
}

