import { createContext, useState } from "react";

export const colorSwitchContext = createContext({});

const Provider = colorSwitchContext.Provider;

export const ColorSwitchProvider = ({ children }) => {
    const [colorA, setColorA] = useState("red");
    const [colorB, setColorB] = useState("blue");

    const [color, setColor] = useState(colorA)

    const handleSwitch = () => {
        setColor(color === colorA ? colorB : colorA);
    }

    const value = {
        color, 
        handleSwitch,
        setColorA,
        setColorB
    }

  return (
    <Provider value={value}>
        {children}
    </Provider>
  );
};
