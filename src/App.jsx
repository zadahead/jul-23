import { Box } from "./Components/Box";
import { Welcome } from "./Components/Welcome";
import { Between, Line, Rows } from "./UIKit/Layouts/Line/Line";

import "./App.css";
import { Btn } from "./UIKit/Elements/Btn/Btn";
import { Icon } from "./UIKit/Elements/Icon/Icon";
import { Route, Routes } from "react-router-dom";
import { Home } from "./Views/Home";
import { About } from "./Views/About";
import { Header } from "./Site/Header";
import { Container } from "./UIKit/Layouts/Container/Container";
import { List } from "./Views/List";
import { LifeCycle, UsersLifeCycle } from "./Views/LifeCycle";
import { Refs } from "./Views/Refs";
import { CHooks } from "Views/CHooks";
import { StatsDisplay } from "Components/StatsDisplay";
import { Callback } from "Components/Callback";

export const App = () => {
  console.log("App");
  
  return (
    <div className="App">
      <Rows>
        <Header />
        <Container>
          <Routes>
            <Route path="/home" element={<Home />} />
            <Route path="/about" element={<About />} />
            <Route path="/list" element={<List />} />
            <Route path="/lifecycle" element={(
              <Rows>
                <UsersLifeCycle />
                <LifeCycle />
              </Rows>
            )} />

            <Route path="/refs" element={<Refs />} />
            <Route path="/hooks" element={<CHooks />} />
            <Route path="/callback" element={<Callback />} />
          </Routes>
        </Container>
      </Rows>

      {/* <Box title="My Title" info="This is a great info">
                <h3>some content</h3>
            </Box>

            <Box
                title="My Title111"
                info="This is a great info111"
            >
                <h4>some content</h4>
            </Box>
            <Welcome>
                <h3>Will this be rendered?</h3>

            </Welcome> */}
    </div>
  );
};
